#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="$(basename $DIR)"
ARCH="-arm-v7"
#CONTAINER_NAME="${DIR}${ARCH}"
CONTAINER_NAME="${DIR}"
TAG="latest${ARCH}"
IMAGE_NAME="app-container-image-telegraf-prebuilt-oci${ARCH}"
