source ../container-name.sh
IMAGE_NAME=$1

if [ $# -lt 1 ];
then
    echo "+ $0: Too few arguments!"
    echo "+ use something like:"
    echo "+ $0 <docker image>"
    echo "+ $0 reliableembeddedsystems/${CONTAINER_NAME}"
    echo "+ $0 reliableembeddedsystems/${CONTAINER_NAME}:${TAG}"
    exit
fi

echo "======================================================="
echo "run this on the target:"
echo "======================================================="
echo "ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
echo "docker ps -a"
echo "docker stop \${ID_TO_KILL}"
echo "docker rm -f \${ID_TO_KILL}"
echo "docker ps -a"
echo "======================================================="
echo "docker pull ${IMAGE_NAME}"
echo "docker run --interactive --tty --entrypoint=/bin/ash ${IMAGE_NAME} --login"
echo "======================================================="
echo "run this in the container:"
echo "======================================================="
echo "phoronix-test-suite"
echo "phoronix-test-suite list-tests"
echo "phoronix-test-suite benchmark pts/scimark2"
echo "======================================================="
